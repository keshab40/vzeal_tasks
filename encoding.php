<?php 

//for example take string AAAABBBCCDAA

$initialString = "AAAABBBCCDAA";

$resultString = runLengthEncoding($initialString);

echo "The encoded string is ".$resultString;

function runLengthEncoding($str){

	$encodedString = "";

	for($i=0; $i<strlen($str); $i++){

		$count = 1;

		while( ($i+1)<strlen($str) && $str[$i] == $str[$i+1] ){

			$count++;

			$i++;

		}

		$encodedString .= $count.$str[$i];
	}

	return $encodedString;
}



?>