<?php 

//Given time interval of classes
$inputTimeInterval = [ [0, 20], [25,45], [50, 90], [95,155] ];

$startTime = [];

$endTime = [];

for($i=0; $i<count($inputTimeInterval); $i++){

	array_push($startTime, $inputTimeInterval[$i][0]);

	array_push($endTime, $inputTimeInterval[$i][1]);
}

//sorting start and end time interval array
sort($startTime);

sort($endTime);

$totalClass = count($startTime);

$room = findMinimumRoomRequired($startTime,$endTime,$totalClass);

echo "Minimum Number of Room required is ".$room;

//calculates minimum room required from start and end arry of time interval
function findMinimumRoomRequired($start,$end,$total){

	$required = 1;

	$resultRoom = 1;

	$s = 1;

	$e = 0;

	while ($s < $total && $e < $total) {

		//checking if any startTime is overlapping to another endTime of class
		if($start[$s] <= $end[$e]){

			$required++;

			$s++;

			if($required > $resultRoom){

				$resultRoom = $required;
			}

		}
		else{

			$required--;

			$e++;
		}
	}

	return $resultRoom;
}




?>